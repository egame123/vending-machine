package com.coffee.model;

public class Recipe {
	private Ingrediant ingrediant;
	private int quantity;
	public Recipe() {
		
	}
	public Recipe(Ingrediant ingrediant, int quantity) {
		super();
		this.ingrediant = ingrediant;
		this.quantity = quantity;
	}
	public Ingrediant getIngrediant() {
		return ingrediant;
	}
	public void setIngrediant(Ingrediant ingrediant) {
		this.ingrediant = ingrediant;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	@Override
	public String toString() {
		return this.ingrediant.toString();
	}
}
