package com.coffee.model;

public class Elaichi extends Ingrediant {
	private static volatile int quantity;
	
	public Elaichi() {
		// TODO Auto-generated constructor stub
	}

	public Elaichi(int quantity) {
		Elaichi.quantity = quantity;
	}

	public synchronized int getQuantity(){
		return quantity;
	}

	public void setQuantity(int quantity) {
		Elaichi.quantity = quantity;
	}
	@Override
	public String toString() {
		return "elaichi";
	}
}
