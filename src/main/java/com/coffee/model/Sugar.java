package com.coffee.model;

public class Sugar extends Ingrediant {
	private static volatile int quantity;

	public Sugar() {
		// TODO Auto-generated constructor stub
	}

	public Sugar(int quantity) {
		Sugar.quantity = quantity;
	}

	public synchronized int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		Sugar.quantity = quantity;
	}
	@Override
	public String toString() {
		return "sugar";
	}
}
