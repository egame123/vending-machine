package com.coffee.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Coffee implements Beverage {
	static List<Recipe> ingrediants = new ArrayList();
	static {

		ingrediants.add(new Recipe(new Water(), 50));
		ingrediants.add(new Recipe(new Milk(), 10));
		ingrediants.add(new Recipe(new CoffeeSyrup(), 10));
		ingrediants.add(new Recipe(new Sugar(), 10));
	}
	public Coffee() {

	}

	@Override
	public void prepare() {
		System.out.println("preparing coffee");
		synchronized (obj) {
			Set<Recipe> listOfExhausetedRecipes = ingrediants.stream()
					.filter(recipe -> recipe.getQuantity() > recipe.getIngrediant().getQuantity())
					.collect(Collectors.toSet());
			if (listOfExhausetedRecipes.size() > 0) {
				System.out.println("black_tea cannot be prepared because " + listOfExhausetedRecipes.toString()
						+ " is not available");
				return;
			}
			try {
				TimeUnit.MILLISECONDS.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ingrediants.stream().forEach(recipe -> recipe.getIngrediant()
					.setQuantity(recipe.getIngrediant().getQuantity() - recipe.getQuantity()));
			System.out.println("coffee is prepared");
			System.out.println(ingrediants.get(2).getQuantity());
		}
	}
}
