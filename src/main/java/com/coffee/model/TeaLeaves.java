package com.coffee.model;

public class TeaLeaves extends Ingrediant {
	private static volatile int quantity;

	public TeaLeaves() {
		// TODO Auto-generated constructor stub
	}

	public TeaLeaves(int quantity) {
		TeaLeaves.quantity = quantity;
	}
	public synchronized int getQuantity(){
		return quantity;
	}

	public synchronized void setQuantity(int quantity) {
		TeaLeaves.quantity = quantity;
	}

	@Override
	public String toString() {
		return "tea_leaves";
	}
}
