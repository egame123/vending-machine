package com.coffee.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

public class ElaichiTea implements Beverage {
	static List<Recipe> ingrediants = new ArrayList();
	@Autowired
	private ReentrantLock reentrantLock;

	static {
		ingrediants.add(new Recipe(new Water(), 50));
		ingrediants.add(new Recipe(new Milk(), 10));
		ingrediants.add(new Recipe(new TeaLeaves(), 10));
		ingrediants.add(new Recipe(new Elaichi(), 5));
		ingrediants.add(new Recipe(new Sugar(), 10));
	}

	@Override
	public synchronized void prepare() {
		System.out.println("preparing elaichi_tea");
		synchronized (obj) {
			Set<Recipe> listOfExhausetedRecipes = ingrediants.stream()
					.filter(recipe -> recipe.getQuantity() > recipe.getIngrediant().getQuantity())
					.collect(Collectors.toSet());
			if (listOfExhausetedRecipes.size() > 0) {
				System.out.println("elaichi_tea cannot be prepared because " + listOfExhausetedRecipes.toString()
						+ " is not available");
				return;
			}

			try {
				TimeUnit.MILLISECONDS.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ingrediants.stream().forEach(recipe -> recipe.getIngrediant()
					.setQuantity(recipe.getIngrediant().getQuantity() - recipe.getQuantity()));
			System.out.println("elaichi_tea is prepared");
		}
	}
}
