package com.coffee.model;

public abstract class Ingrediant {
	public abstract int getQuantity();
	public abstract void setQuantity(int quantity);
}
