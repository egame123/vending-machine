package com.coffee.model;

public class Ginger extends Ingrediant {
	private static volatile int quantity;
	
	public Ginger() {
		// TODO Auto-generated constructor stub
	}

	public Ginger(int quantity) {
		Ginger.quantity = quantity;
	}

	public synchronized int getQuantity() {
		return quantity;
	}

	public synchronized void setQuantity(int quantity) {
		Ginger.quantity = quantity;
	}
	@Override
	public String toString() {
		return "ginger";
	}
}
