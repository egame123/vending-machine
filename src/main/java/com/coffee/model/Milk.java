package com.coffee.model;

public class Milk extends Ingrediant {
	private static volatile int quantity;

	public Milk() {
		// TODO Auto-generated constructor stub
	}

	public Milk(int quantity) {
		Milk.quantity = quantity;
	}
	public synchronized int getQuantity() {
		return quantity;
	}

	public synchronized void setQuantity(int quantity) {
		Milk.quantity = quantity;
	}
	@Override
	public String toString() {
		return "milk";
	}
}
