package com.coffee.model;

public class CoffeeSyrup extends Ingrediant {
	private static volatile int quantity;

	public CoffeeSyrup() {
		// TODO Auto-generated constructor stub
	}

	public CoffeeSyrup(int quantity) {
		CoffeeSyrup.quantity = quantity;
	}

	public synchronized int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		CoffeeSyrup.quantity = quantity;
	}
	@Override
	public String toString() {
		return "coffee_syrup";
	}
}
