package com.coffee.model;

public class Water extends Ingrediant {
	private static volatile int quantity;

	public Water() {
		// TODO Auto-generated constructor stub
	}

	public Water(int quantity) {
		Water.quantity = quantity;
	}

	public synchronized int getQuantity() {
		return quantity;

	}

	public synchronized void setQuantity(int quantity) {
		Water.quantity = quantity;
	}

	@Override
	public String toString() {
		return "water";
	}

}
