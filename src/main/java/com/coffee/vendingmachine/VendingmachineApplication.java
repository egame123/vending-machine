package com.coffee.vendingmachine;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.coffee.model.Beverage;
import com.coffee.model.Coffee;
import com.coffee.model.CoffeeSyrup;
import com.coffee.model.Elaichi;
import com.coffee.model.ElaichiTea;
import com.coffee.model.Ginger;
import com.coffee.model.GingerTea;
import com.coffee.model.HotMilk;
import com.coffee.model.HotWater;
import com.coffee.model.Ingrediant;
import com.coffee.model.Milk;
import com.coffee.model.Sugar;
import com.coffee.model.TeaLeaves;
import com.coffee.model.Water;

@SpringBootApplication(scanBasePackages= {"com.coffee"} )
public class VendingmachineApplication {

	public static void main(String[] args) {
		SpringApplication.run(VendingmachineApplication.class, args);
		Ingrediant water = new Water(100);
		Ingrediant milk = new Milk(100);
		Ingrediant teaLeaves = new TeaLeaves(100);
		Ingrediant gingerSyrup = new Ginger(100);
		Ingrediant sugar = new Sugar(100);
		Ingrediant elaichi = new Elaichi(100);
		Ingrediant coffeeSyrup = new CoffeeSyrup(100);
		Beverage hotWater = new HotWater();
		Beverage coffee = new Coffee();
		Beverage elaichiTea = new ElaichiTea();
		Beverage gingerTea = new GingerTea();
		Beverage hotMilk = new HotMilk();
		int N = 1;
		Scanner sc = new Scanner(System.in);
		while (true) {
			try {

				System.out.println("Enter Outlets: ");
				N = sc.nextInt();
				break;
			} catch (Exception e) {
				System.out.println("Invalid Outlets");
			}
		}
		ExecutorService executorService = Executors.newFixedThreadPool(N);

		Queue<Callable<String>> tasks = new LinkedList();
		while (true) {
			int choice = menu();
			tasks = new LinkedList<>();
			switch (choice) {
			case 1:
				process(gingerTea, executorService);
				break;
			case 2:
				process(elaichiTea, executorService);
				break;
			case 3:
				process(coffee, executorService);
				break;
			case 4:
				process(hotMilk, executorService);
				break;
			case 5:
				process(hotWater, executorService);
				break;
			case 6:
				System.out.println("Enter Water Quantity");
				int waterQuantity=sc.nextInt();
				water.setQuantity(water.getQuantity()+waterQuantity);
				break;
			case 7:
				System.out.println("Enter Milk Quantity");
				int milkQuantity=sc.nextInt();
				milk.setQuantity(milk.getQuantity()+milkQuantity);
				break;
			case 8:
				System.out.println("Enter Ginger Quantity");
				int gingerQuantity=sc.nextInt();
				gingerSyrup.setQuantity(gingerSyrup.getQuantity()+gingerQuantity);
				break;
			case 9:
				System.out.println("Enter Elaichi Quantity");
				int elaichiQuantity=sc.nextInt();
				elaichi.setQuantity(elaichi.getQuantity()+elaichiQuantity);
				break;
			case 10:
				System.out.println("Enter Coffee Syrup Quantity");
				int quantity=sc.nextInt();
				coffeeSyrup.setQuantity(coffeeSyrup.getQuantity()+quantity);
				break;
			default:
				System.out.println("Invalid Input");
				break;
			}
		}
	}
	
	public static void execute(final Queue<Callable<String>> task, ExecutorService executorService) {
		CompletableFuture.supplyAsync(() -> {
			try {
				return executorService.invokeAll(task);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		});
	}
	
	public static void process(Beverage beverage, ExecutorService executorService) {
		Queue<Callable<String>> tasks = new LinkedList<>();
		Callable<String> callableTask1 = () -> {
			beverage.prepare();
			return "Task's execution";
		};
		tasks.add(callableTask1);
		final Queue<Callable<String>> task = tasks;
		execute(task, executorService);
	}

	private static int menu() {
		System.out.println("*****Menu*****\n" 
				+ "1. ginger tea\n" 
				+ "2. elaichi tea\n" 
				+ "3. coffee\n" 
				+ "4. hot milk\n"
				+ "5. hot water\n"
				+ "6. refill water\n"
				+ "7. refill milk\n"
				+ "8. refill ginger\n"
				+ "9. refill elaichi\n"
				+ "10. refill coffee\n");
		Scanner sc = new Scanner(System.in);
		int input = -1;
		try {
			input = sc.nextInt();
		} catch (Exception e) {
		}
		return input;
	}

}
