package com.coffee.vendingmachine;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.coffee.model.Beverage;
import com.coffee.model.Coffee;
import com.coffee.model.CoffeeSyrup;
import com.coffee.model.Elaichi;
import com.coffee.model.ElaichiTea;
import com.coffee.model.Ginger;
import com.coffee.model.GingerTea;
import com.coffee.model.HotMilk;
import com.coffee.model.HotWater;
import com.coffee.model.Ingrediant;
import com.coffee.model.Milk;
import com.coffee.model.Sugar;
import com.coffee.model.TeaLeaves;
import com.coffee.model.Water;

@SpringBootTest
class VendingmachineApplicationTests {

	Beverage hotWater = new HotWater();
	Beverage coffee = new Coffee();
	Beverage elaichiTea = new ElaichiTea();
	Beverage gingerTea = new GingerTea();
	Beverage hotMilk = new HotMilk();
	Ingrediant water = new Water(500);
	Ingrediant milk = new Milk(500);
	Ingrediant teaLeaves = new TeaLeaves(500);
	Ingrediant gingerSyrup = new Ginger(500);
	Ingrediant sugar = new Sugar(500);
	Ingrediant elaichi = new Elaichi(500);
	Ingrediant coffeeSyrup = new CoffeeSyrup(10);
	Callable<String> callableTask1 = () -> {
		coffee.prepare();
		return "Task's execution";
	};
	Callable<String> callableTask2 = () -> {
		gingerTea.prepare();
		return "Task's execution";
	};
	Callable<String> callableTask3 = () -> {
		hotWater.prepare();
		return "Task's execution";
	};
	Callable<String> callableTask4 = () -> {
		hotWater.prepare();
		return "Task's execution";
	};
	
	@BeforeEach
	public void init() {
		hotWater = new HotWater();
		coffee = new Coffee();
		elaichiTea = new ElaichiTea();
		gingerTea = new GingerTea();
		hotMilk = new HotMilk();
		water = new Water(500);
		milk = new Milk(500);
		teaLeaves = new TeaLeaves(500);
		gingerSyrup = new Ginger(500);
		sugar = new Sugar(500);
		elaichi = new Elaichi(500);
		coffeeSyrup = new CoffeeSyrup(10);
	}
	
	@Test
	void testValid3Outlets4Beverages() throws InterruptedException {
		ExecutorService executorService = Executors.newFixedThreadPool(3);
		Queue<Callable<String>> tasks = new LinkedList();
		
		tasks.add(callableTask1);
		tasks.add(callableTask2);
		tasks.add(callableTask3);
		tasks.add(callableTask4);
		executorService.invokeAll(tasks);
		assertTrue(water.getQuantity() == 300);
	}
	
	@Test
	void testValid3Outlets3Beverages() throws InterruptedException {
		ExecutorService executorService = Executors.newFixedThreadPool(3);
		Queue<Callable<String>> tasks = new LinkedList();
		
		tasks.add(callableTask1);
		tasks.add(callableTask2);
		tasks.add(callableTask3);
		executorService.invokeAll(tasks);
		assertTrue(water.getQuantity() == 350);
	}
	
	@Test
	void testWaterNotAvailable() throws InterruptedException {
		ExecutorService executorService = Executors.newFixedThreadPool(3);
		Queue<Callable<String>> tasks = new LinkedList();
		water=new Water(100);
		tasks.add(callableTask1);
		tasks.add(callableTask2);
		tasks.add(callableTask3);
		tasks.add(callableTask4);
		executorService.invokeAll(tasks);
		assertTrue(water.getQuantity() == 0);
	}
	
	@Test
	void testCoffeeNotAvailable() throws InterruptedException {
		ExecutorService executorService = Executors.newFixedThreadPool(3);
		Queue<Callable<String>> tasks = new LinkedList();
		coffeeSyrup=new CoffeeSyrup(10);
		tasks.add(callableTask1);
		tasks.add(callableTask1);
		executorService.invokeAll(tasks);
		assertTrue(coffeeSyrup.getQuantity() == 0);
	}
}
